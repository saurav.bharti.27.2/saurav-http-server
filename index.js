
const http = require('node:http')

const fs = require('fs-extra')

const path = require('path')

const uuid= require('uuid')


const file = './index.html'

const port = process.env.PORT || 8000

const file_path = path.join(__dirname, file)

const json_string = {
    "slideshow": {
      "author": "Yours Truly",
      "date": "date of publication",
      "slides": [
        {
          "title": "Wake up to WonderWidgets!",
          "type": "all"
        },
        {
          "items": [
            "Why <em>WonderWidgets</em> are great",
            "Who <em>buys</em> WonderWidgets"
          ],
          "title": "Overview",
          "type": "all"
        }
      ],
      "title": "Sample Slide Show"
    }
  }
  

function generate_random_uuid(){

    const uuid_string =  uuid.v4()

    return uuid_string
}

const server = http.createServer((req, res) => {

    
    if(req.url === '/html'){

        res.writeHead(200, {"Content-Type" : "text/html" } )    

        fs.createReadStream(file_path).pipe(res)
        
    }

    else if(req.url === "/json"){

        res.writeHead(200, {'Content-Type' : 'application/json'})

        res.write(JSON.stringify(json_string))

        res.end()
    }

    else if(req.url === '/uuid'){

        res.writeHead(200, {'Content-Type' : 'application/json'})

        res.write(JSON.stringify(`uuid : ${generate_random_uuid()}`))

        res.end()
    }
    else{

        const parsed_url = req.url.split('/')

        if(parsed_url[1] === 'status'){
            
            try{

               
                if(Number(parsed_url[2]) >= 1000 || Number(parsed_url[2])<100){
    
                    res.writeHead(422, {'Content-Type' : 'text/plain'})
    
                    res.write("Entered status code is invalid")
    
                    res.end()
                }
                else{
                    
                    res.writeHead( Number(parsed_url[2]) )
        
                    res.end()
                }

            }
            catch(err){

                res.writeHead(422, {'Content-Type' : 'text/plain'})

                res.write(JSON.stringify( err))

                res.end()
            }

        }

        else if(parsed_url[1] === 'delay'){
            
            try{

                if(isNaN(parsed_url[2])){

                    throw ("Enter valid time delay")

                }
            
                let delay_time= Number(parsed_url[2])

                
                setTimeout(() => {

                    res.writeHead(200, {'Content-Type' : 'text/plain'})

                    res.write(`Success after ${delay_time} seconds`)

                    res.end()

                }, delay_time* 1000)



            }
            catch(err){
                
                res.writeHead(422, {'Content-Type' : 'application/json'})
                
                res.write(JSON.stringify(err))

                res.end()
            }

        }
        else{

            res.writeHead( 404)            
            
            res.end()

        }


    }


}).listen(port, ()=>{

    console.log(`Listening to port ${port}`)
    
})

